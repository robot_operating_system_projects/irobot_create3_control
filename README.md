# iRobot Create3 control 

This package, targeted for ROS 2 Galactic environment, offers a controller for iRobot Create 3 to perform a drawing of a selected letter. Make sure ROS 2 is installed on your machine.

**Link to the video**: https://youtu.be/6B-ygos_BEo

## Installation

In your ROS 2 workspace, make sure that [iRobot Create 3 simulation](https://github.com/iRobotEducation/create3_sim/tree/galactic) is cloned in src directory, that you followed all the steps to set it up and that you are sourced to Galactic ```setup.bash``` file. After that, clone this repository, build the project and launch the system:

```
cd your_workspace/src
git clone https://gitlab.com/robot_operating_system_projects/irobot-create3-control.git
cd ..
colcon build --symlink-install
source install/local_setup.bash
ros2 launch irobot_create3_control system_bringup.launch.py
```

## Repository content

The repository contains the controller program, which performs the following actions:

1. Undocks the robot from the docking station.
2. Navigates the robot to the predefined points forming a letter (in this example, the letter E). While performing this step, a marker with an array of points are sent to a topic, which is then visualized in rviz to show the action of "drawing" a letter.
3. After finishing the drawing, the robot goes to the beginning position facing the docking station and tries to dock (though there is a problem with that, explained in detail further).

#

### **config/letter.yaml**

The file contains two parameters for the *controller* node (*letter_x* and *letter_y* lists), which correspond to the *x,y* coordinates of points, which (after visited in the provided sequence) form a letter. However, the points are not provided in the global coordinate frame - they are offsets to the starting position.

**Q**: Why not to create a list of lists as a parameter instead of holding x, y in separate lists?

**A**: Honestly, I do not know how to later decode it - I only found **.double_array_value.tolist()* decoding in the node, I tried with nested lists, but it didn't work. If there is a workaround, I would gladly get to know it. 

#

### **launch/system_bringup.launch.py**

This file launches both the Create 3 gazebo simulation and the custom controller node with the parameters from the .yaml file.

#

### **irobot_create3_control/controller.py**

This node consists of:

* **action clients**: Dock, Undock and NavigateToPosition,
* **subscribers**: *dock*, *odom* and *clock*,
* **publishers**: *letter* markers,
* **parameters**: *letter_x* and *letter_y* (as mentioned above, these are offsets to the starting position, used to create a letter drawing).

To ensure that the correct sequence of actions is followed, I created an enum defining the states with the corresponding steps happening in between.

#### **UNINITIALIZED**
1. Get the letter *x,y* offsets lists from parameter server, merge them to one list.
2. Set the controller state to INITIALIZED.

#### **INITIALIZED**
1. If the message from *dock* topic states that the robot is docked, send the goal to the action server to undock the robot.
2. If the server responds with the success of the robot undocking action, set the state to UNDOCKED.

#### **UNDOCKED**
1. Create the list with poses of consecutive letter positions by adding the offset from the parameters to the current robot's position.
2. Save the current position as robot's last one, with the only difference of being rotated by 180 degrees (to assure that the robot faces the docking station).
3. Set the controller state to READY_TO_DRAW.

#### **READY_TO_DRAW**
1. Move the robot to the first position of the letter drawing sequence.
2. Increment the iteration counter by 1 (later used to go over the list of desired poses), set the *next* flag to true (for the controller to know that the next pose can be sent to the action server).
3. Set the controller state to DRAWING. 

#### **DRAWING**
1. As long as the iteration counter (incremented after each reached pose from poses list) is still within the pose list range, keep navigating to next poses. What is important, is that the actions are not overridden, so the flag *next* is set either to false or true, depending if the previous navigation action was already processed.
2. At the same time, keep saving the robot's actual poses in an array and publish them as a marker, to visualize the sweep it in rviz.
3. If the iteration counter exceeds the length of the pose list, set the controller state to DRAWING_FINISHED.

#### **DRAWING_FINISHED**
1. Navigate to the last, pre-docking pose, saved in UNDOCKED stage.
2. Set the controller state to READY_TO_DOCK. 

#### **READY_TO_DOCK**
1. If the dock is visible, change the status to DOCKING.

#### **DOCKING**
1. Send the docking goal to the action server.
2. If the server responds with a success information, set the controller state to DOCKED.

#### **DOCKED**
End of the program.

## Known problems, possible solutions

### 1. Number of points in the published marker
I am aware that I add a huge number of points to the marker, which is then published in rviz. I believe the lag that can be seen in the video is due to this problem. My markers were overridden if I did not publish them as a list, and one solution could be to add only e.g. every 10th point to the list.

### 2. Subscription to the clock
I do not believe it is a clean solution, I just needed an equivalent of while(True) function which would not stop other processes in the node. I know there has to be a better way, but this seemed to work, so I stayed with it. 

### 3. Using enum for defining the stages of the node
As in point 2, there has to be a fancier to it. However, none of the ROS courses I took taught the clean architecture design in ROS (not mentioning ROS 2 at all), so I am not sure how to do it.

### 4. Letter positions in robot's frame
Currently, letter points in yaml file are offsets to the starting position. To not hit the dock station, I needed to set them to negative in some cases. It would be much better to have them as offsets, but take into consideration the robot orientation at the starting pose (as we know that the robot doesn't face the docking station after undocking) and adjust the offsets to the robot's front (e.g. using the transformations library).

### 5. Navigation action success confimration
For now, it is assumed the navigation to position ended with a success if it is simply executed. The action server returns the robot final position after performing the action, so to actually confirm it was successful, the desired and actual poses of the robot should be compared.

### 6. Docking

Ah yes, it does not work. I had a couple of ideas and all of them failed. Currently, the docking service can be executed only if the docking station is visible to the robot. To ensure that, I saved the starting robot position (as after undocking the robot is rotated in the opposite direction) and rotated it by 180 degrees. The problems are that apparently the *dock* topic data in *is_docked* field changes to False when the robot's back is not exactly facing the docking station. Therefore, in my last pose robot is slightly rotated to the side and apparently the docking service orders the robot to move forward (does not account for slight rotations).

I also thought about staying in the last letter position and rotating in place (by using cmd_vel topic) until the docking station is visible, but turned out to be working much worse than the above idea.

The solution would be to ensure that in the firstly described approach, the difference in yaw angle of the robot pose and the angle of the vector (robot_position, docking_station_position) is zero, and only then to call the action of docking. 
