import sys

from enum import Enum
from math import pi

import rclpy
from rclpy.node import Node

from rclpy.action import ActionClient
from irobot_create_msgs.action import Undock, NavigateToPosition
from irobot_create_msgs.action import DockServo as DockAct
from irobot_create_msgs.msg import Dock
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseStamped, Point, Twist, Pose
from visualization_msgs.msg import Marker
from rosgraph_msgs.msg import Clock

from tf_transformations import quaternion_from_euler, quaternion_multiply



class State(Enum):
    UNINITIALIZED = -1
    INITIALIZED = 0
    UNDOCKED = 1
    READY_TO_DRAW = 2
    DRAWING = 3
    DRAWING_FINISHED = 4
    READY_TO_DOCK = 5
    DOCKING = 6
    DOCKED = 7



class Controller(Node):

    def __init__(self):
        super().__init__('controller')
        
        self.undock_client_   = ActionClient(self, Undock,  'undock')
        self.dock_client_     = ActionClient(self, DockAct, 'dock')
        self.navigate_client_ = ActionClient(self, NavigateToPosition, 'navigate_to_position')

        self.dock_status_sub = self.create_subscription(Dock,     'dock',  self.dock_status_callback, 10)
        self.odom_sub        = self.create_subscription(Odometry, 'odom',  self.odom_callback, 10)
        self.clock_sub       = self.create_subscription(Clock,    'clock', self.clock_callback, 10)

        self.draw_pub = self.create_publisher(Marker, 'letter', 10)

        self.state_ = State.UNINITIALIZED

        self.marker_array_ = []
        self.last_pose_    = None
        self.next_         = False
        self.it_           = 0

        self.declare_parameter('letter_x', [0.0, 0.0])
        self.declare_parameter('letter_y', [0.0, 0.0])
        
        self.letter_points_rel_  = []
        self.letter_points_glob_ = []
        
        self.get_params()
                
    def clock_callback(self, msg):
        if self.state_ == State.DRAWING and self.next_ and self.it_ < len(self.letter_points_glob_):
            self.next_ = False # to make sure we don't process another point till this one is finished
            self.navigate(self.letter_points_glob_[self.it_])

        if self.state_ == State.DRAWING and self.it_ >= len(self.letter_points_glob_):
            self.state_ = State.DRAWING_FINISHED
            self.get_logger().info('DRAWING FINISHED')
            self.navigate(self.last_pose_)     
    
    def odom_callback(self, msg):
        pose = msg.pose.pose.position
        q = msg.pose.pose.orientation

        if self.state_ == State.UNDOCKED:

            for point in self.letter_points_rel_:
                new_pose = Pose()
                new_pose.position.x = pose.x+point[0]
                new_pose.position.y = pose.y+point[1]
                self.letter_points_glob_.append(new_pose)

            self.next_ = True

            # take the first pose after undocking, rotate it by 180 degrees and 
            # save it as the last pose (the one just before docking back)
            self.last_pose_ = Pose()
            q_origin = [q.x, q.y, q.z, q.w]
            q_rot = quaternion_from_euler(0, 0, pi)
            quat = quaternion_multiply(q_rot, q_origin)
            self.last_pose_.position.x = pose.x
            self.last_pose_.position.y = pose.y
            self.last_pose_.position.z = pose.z
            self.last_pose_.orientation.x = quat[0]
            self.last_pose_.orientation.y = quat[1]
            self.last_pose_.orientation.z = quat[2]
            self.last_pose_.orientation.w = quat[3]

            # navigate to the first point without drawing
            self.state_ = State.READY_TO_DRAW
            self.navigate(self.letter_points_glob_[self.it_])
            

        if self.state_ == State.DRAWING:
            point = Point()
            point.x = pose.x
            point.y = pose.y
            self.marker_array_.append(point)

            marker = Marker()
            marker.header.frame_id = "odom"
            marker.type = Marker.POINTS
            marker.action = Marker.ADD
            marker.scale.x = 0.1
            marker.scale.y = 0.1
            marker.scale.z = 0.1
            marker.color.a = 1.0
            marker.color.r = 1.0
            marker.color.g = 0.0
            marker.color.b = 0.0
            marker.points = self.marker_array_

            self.draw_pub.publish(marker)
        
    def get_params(self):
        letter_x = self.get_parameter('letter_x').get_parameter_value().double_array_value.tolist()
        letter_y = self.get_parameter('letter_y').get_parameter_value().double_array_value.tolist()
        for x, y in zip(letter_x, letter_y):
            self.letter_points_rel_.append([x,y])
        
        self.state_ = State.INITIALIZED

    ######################################## 

    def navigate(self, pose):
        self.navigate_client_.wait_for_server()
        goal = NavigateToPosition.Goal()
        goal_pose = PoseStamped()
        goal_pose.pose = pose
        goal.goal_pose = goal_pose
        goal_future_ = self.navigate_client_.send_goal_async(goal)
        goal_future_.add_done_callback(self.navigate_response_callback)

    def navigate_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            return

        get_result_future = goal_handle.get_result_async()
        get_result_future.add_done_callback(self.navigate_result_callback)

    def navigate_result_callback(self, future):
        result = future.result().result
        self.next_ = True
        if self.state_ == State.READY_TO_DRAW:
            self.it_ += 1
            self.state_ = State.DRAWING
        elif self.state_ == State.DRAWING:
            self.it_ += 1
        elif self.state_ == State.DRAWING_FINISHED:
            self.state_ = State.READY_TO_DOCK
        
    ########################################    

    def dock_status_callback(self, msg):
        if self.state_ == State.INITIALIZED and msg.is_docked:
            self.undock()
        if self.state_ == State.READY_TO_DOCK and msg.dock_visible:
            self.state_ = State.DOCKING
            self.dock()

    def dock(self):
        self.dock_client_.wait_for_server()
        goal = DockAct.Goal()
        goal_future_ = self.dock_client_.send_goal_async(goal)
        goal_future_.add_done_callback(self.dock_response_callback)

    def undock(self):
        self.undock_client_.wait_for_server()
        goal = Undock.Goal()
        self.goal_future_ = self.undock_client_.send_goal_async(goal)
        self.goal_future_.add_done_callback(self.dock_response_callback)

    def dock_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            return

        get_result_future = goal_handle.get_result_async()
        get_result_future.add_done_callback(self.dock_result_callback)

    def dock_result_callback(self, future):
        result = future.result().result
        if not result.is_docked and self.state_ == State.INITIALIZED:
            self.state_ = State.UNDOCKED
            self.get_logger().info('Robot is undocked')
        elif result.is_docked and self.state_ == State.DOCKING:
            self.state_ = State.DOCKED
            self.get_logger().info('Robot is docked')

    ########################################

    def kill_node(self):    
        self.destroy_node()
        sys.exit(0)


def main(args=None):
    rclpy.init(args=args)

    controller = Controller()
    rclpy.spin(controller)

    controller.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
