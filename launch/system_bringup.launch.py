from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
import launch
import os

def generate_launch_description():
    ld = LaunchDescription()
    irobot_gazebo_dir = get_package_share_directory('irobot_create_gazebo_bringup')
    gazebo_launch = launch.actions.IncludeLaunchDescription(
        launch.launch_description_sources.PythonLaunchDescriptionSource(
                irobot_gazebo_dir + '/launch/create3_gazebo.launch.py'))

    config = os.path.join(
        get_package_share_directory('irobot_create3_control'),
        'config',
        'letter.yaml'
        )
    

    controller_node = Node(
        package = 'irobot_create3_control',
        executable = 'controller',
        name = 'controller',
        output = 'screen',
        emulate_tty = True,
        parameters = [config]
    )

    return LaunchDescription([
        gazebo_launch,
        controller_node,
    ])
