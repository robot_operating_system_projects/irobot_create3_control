from setuptools import setup
import os
from glob import glob

package_name = 'irobot_create3_control'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*.yaml')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='emilia',
    maintainer_email='emilia.szymanska.eka@gmail.com',
    description='Controller for iRobot Create 3 to perform a drawing of a selected letter.',
    license='MIT License',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'controller = irobot_create3_control.controller:main',
        ],
    },
)
